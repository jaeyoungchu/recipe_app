import 'package:recipe_app/models/recipe_detail_model.dart';
import 'package:recipe_app/models/procedure_model.dart';
import 'package:recipe_app/models/recipe_simple_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class HashMapManager {

  RecipeDetail getDetailModelFromDocument(DocumentSnapshot map){

    String recipeName = map['recipeName'];
    String author = map['author'];
    String imageUrl = map['imageUrl'];
    String recipeType = map['recipeType'];
    String token = map['token'];
    String authEmail = map['authEmail'];
    int likeCount = map['likeCount'];
    int favoriteCount = map['favoriteCount'];

    List<String> ingredientList = List();

    Map<dynamic,dynamic> ingredientListMap = map['ingredientList'];
    bool flag = true;
    int i = 0;
    do{
      if(ingredientListMap['ingredients'+'$i'] == null){
        flag = false;
        break;
      }else{
        ingredientList.add(ingredientListMap['ingredients'+'$i']);
      }
      i++;
    }while(flag);

    Map<dynamic,dynamic> directionListMap = map['directionList'];
    List<ProcedureModel> directionList = List();
    flag = true;
    i = 0;
    do{
      if(directionListMap['directions'+'$i'] == null){
        flag = false;
        break;
      }else{
        directionList.add(ProcedureModel(directionListMap['directions'+'$i']['alarm'], directionListMap['directions'+'$i']['detail']));
      }
      i++;
    }while(flag);

    return RecipeDetail(recipeName,author,imageUrl,recipeType,token,authEmail,likeCount,favoriteCount,ingredientList,directionList);

  }

  Map<String, dynamic> createRecipeDetailDocument(RecipeDetail recipeDetail){
    Map<String,dynamic> recipeDetailMap = new Map();

    recipeDetailMap['recipeName'] = recipeDetail.recipeName;
    recipeDetailMap['author'] = recipeDetail.author;
    recipeDetailMap['imageUrl'] = recipeDetail.imageUrl;
    recipeDetailMap['recipeType'] = recipeDetail.recipeType;
    recipeDetailMap['token'] = recipeDetail.token;
    recipeDetailMap['authEmail'] = recipeDetail.authEmail;
    recipeDetailMap['likeCount'] = recipeDetail.likeCount;
    recipeDetailMap['favoriteCount'] = recipeDetail.favoriteCount;

    if(recipeDetail.ingredientList.length !=0){
      Map<String,dynamic> ingredientsMap = new Map();
      for(int i = 0; i < recipeDetail.ingredientList.length ; i++){
        ingredientsMap['ingredients$i'] = recipeDetail.ingredientList[i];
      }
      recipeDetailMap['ingredientList'] = ingredientsMap;
    }

    if(recipeDetail.directionList.length != 0){
      Map<String,dynamic> directionsMap = new Map();
      for(int i = 0; i < recipeDetail.directionList.length ; i++){

        directionsMap['directions$i'] = _createDirectionMap(recipeDetail.directionList[i]);
      }
      recipeDetailMap['directionList'] = directionsMap;
    }

    return recipeDetailMap;
  }

  RecipeSimpleModel getSimpleModelFromMap(DocumentSnapshot map){
    return RecipeSimpleModel(map['name'],map['type'],map['author'],map['imageUrl'],map['token'],map['authEmail'],map['like'],map['favorite']);
  }

  Map<String, dynamic> createSimpleRecipeDetailMap(RecipeSimpleModel recipeSimpleModel){
    Map<String,dynamic> recipeSimpleModelMap = new Map();

    recipeSimpleModelMap['name'] = recipeSimpleModel.name;
    recipeSimpleModelMap['type'] = recipeSimpleModel.type;
    recipeSimpleModelMap['author'] = recipeSimpleModel.author;
    recipeSimpleModelMap['authEmail'] = recipeSimpleModel.authEmail;
    recipeSimpleModelMap['imageUrl'] = recipeSimpleModel.imageUrl;
    recipeSimpleModelMap['token'] = recipeSimpleModel.token;
    recipeSimpleModelMap['like'] = recipeSimpleModel.like;
    recipeSimpleModelMap['favorite'] = recipeSimpleModel.favorite;
    recipeSimpleModelMap['time'] = DateTime.now().millisecondsSinceEpoch;

    return recipeSimpleModelMap;
  }



  Map<String, dynamic> _createDirectionMap(ProcedureModel procedureModel){
    Map<String,dynamic> procedureModelMap = new Map();

    procedureModelMap['alarm'] = procedureModel.alarm;
    procedureModelMap['detail'] = procedureModel.detail;

    return procedureModelMap;
  }

}