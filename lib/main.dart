import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'screens/login_screen.dart';
import 'screens/signup_screen.dart';
import 'screens/my_recipe_screen.dart';
import 'screens/world_recipe_screen.dart';
import 'screens/create_recipe_screen.dart';

void main(){runApp(MyApp());}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {


    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);

    return MaterialApp(
      title: 'Hello Cook.',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          primarySwatch: Colors.orange
      ),
      routes: <String,WidgetBuilder>{
        '/login_screen' : (BuildContext context) => LoginScreen(),
        '/signup_screen' : (BuildContext context) => SignUpScreen(),
        '/my_recipe_screen' : (BuildContext context) => MyRecipeScreen(),
        '/world_recipe_screen' : (BuildContext context) => WorldRecipeScreen(),
        '/create_recipe_screen' : (BuildContext context) => CreateScreen(),
      },
      home : LoginScreen(),
    );
  }
}