import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final GoogleSignIn googleSignIn = new GoogleSignIn();

  String _email,_name;

  _DrawerWidgetState(){
    _getUser();
  }


  @override
  void initState() {
    super.initState();
    _email = 'loading..';
    _name =  'loading..';
  }

  _getUser() async{
    await FirebaseAuth.instance.currentUser().then((user){
      Firestore.instance.collection('userInfo').document(user.email).get().then((documentsnapshot){
        if(documentsnapshot.exists){
          setState(() {
            _email = documentsnapshot.data['email'];
            _name = documentsnapshot.data['name'];
          });
        }else{
          setState(() {
            _email = 'loading..';
            _name = 'loading..';
          });
        }
      });
    });


  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountEmail: Container(
                color: Colors.black12,
                child: Text(
                  _email,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0),
                ),
              ),
              accountName: Container(
                  color: Colors.black12,
                  child: Text(
                    _name,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0),
                  )),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/drawer_header1.jpg'),
                    fit: BoxFit.fill),
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.favorite,
                color: Colors.orange,
              ),
              title: Text('My Recipe'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/my_recipe_screen', (Route<dynamic> route) => false);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.computer,
                color: Colors.orange,
              ),
              title: Text('World Recipe'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/world_recipe_screen', (Route<dynamic> route) => false);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.create,
                color: Colors.orange,
              ),
              title: Text('Create Recipe'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/create_recipe_screen', (Route<dynamic> route) => false);
              },
            ),
            Divider(
              indent: 15.0,
              color: Colors.orange[400],
            ),
            ListTile(
              leading: Icon(
                Icons.cloud_off,
                color: Colors.orange,
              ),
              title: Text('Sign Out'),
              onTap: () =>_signOut(context),
            ),
          ],
        ),
      ),
    );
  }

  _signOut(BuildContext context){
    googleSignIn.signOut();
    Navigator.pop(context);
    Navigator.of(context).pushNamedAndRemoveUntil(
        '/login_screen', (Route<dynamic> route) => false);
  }
}

