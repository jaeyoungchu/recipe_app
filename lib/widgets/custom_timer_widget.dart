import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:audioplayers/audio_cache.dart';

class CustomTimeWidget extends StatefulWidget {
  final double fontSize,widgetSize;
  final int minute;


  CustomTimeWidget(this.fontSize, this.widgetSize, this.minute);

  @override
  _TimerState createState() => _TimerState(fontSize,widgetSize,minute);
}

class _TimerState extends State<CustomTimeWidget> with TickerProviderStateMixin {
  AnimationController controller;
  double fontSize,widgetSize;
  int durationMinute;
  AudioCache player = new AudioCache();

  _TimerState(this.fontSize, this.widgetSize, this.durationMinute);

  String get timerString {
    Duration duration = controller.duration * controller.value;

    if(controller.value == 0.0){
      Duration duration = controller.duration;
      return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
    }
    if(controller.value < 0.005){
      player.play('alarm_sound.wav');
    }
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(minutes: durationMinute),
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Container(
        height: 80.0,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if (controller.isAnimating) {
                      controller.stop();
                    } else {
                      controller.reverse(
                          from: controller.value == 0.0 ? 1.0 : controller.value);
                    }
                  },
                  child: Align(
                    alignment: FractionalOffset.center,
                    child: AspectRatio(
                      aspectRatio: 1.0,
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                            child: AnimatedBuilder(
                              animation: controller,
                              builder: (BuildContext context, Widget child) {
                                return new CustomPaint(
                                  painter: TimerPainter(
                                    animation: controller,
                                    backgroundColor: Colors.white,
                                    color: themeData.indicatorColor,
                                    strokeWidth: 10.0,
                                  ),
                                );
                              },
                            ),
                          ),
                          Align(
                            alignment: FractionalOffset.center,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
//                            Text('Count Down',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0),),
                                AnimatedBuilder(
                                  animation: controller,
                                  builder: (BuildContext context, Widget child) {
                                    return new Text(
                                      timerString,
                                      style: TextStyle(
                                        fontFamily: 'Mail',
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20.0),
                                    );
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    player.clearCache();
    super.dispose();

  }
}

class TimerPainter extends CustomPainter {
  TimerPainter(
      {this.animation, this.backgroundColor, this.color, this.strokeWidth})
      : super(repaint: animation);

  final Animation<double> animation;
  final Color backgroundColor, color;
  final double strokeWidth;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color;
    double progress = (1.0 - animation.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
