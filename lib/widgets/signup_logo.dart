import 'package:flutter/material.dart';


class SignUpLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpringFallingLogo();
  }
}

class SpringFallingLogo extends StatefulWidget {
  @override
  _SpringFallingLogoState createState() => _SpringFallingLogoState();
}

class _SpringFallingLogoState extends State<SpringFallingLogo>
    with SingleTickerProviderStateMixin {

  Animation<double> animation;
  AnimationController controller;

  @override
  initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 2000),vsync: this);

    animation = Tween(begin: 20.0, end: 40.0).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.bounceOut,
    ));
    animation.addListener(_animationListener);
    controller.forward();
  }
  _animationListener(){
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(-0.7, 0.0),
      child: Text(
        'SignUp',
        style: TextStyle(
            fontSize: animation.value,
            fontWeight: FontWeight.bold,
            color: Colors.orange),
      ),
    );
  }



  dispose() {
    animation.removeListener(_animationListener);
    controller.dispose();
    super.dispose();
  }
}
