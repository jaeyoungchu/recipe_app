import 'package:flutter/material.dart';
//import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/animation.dart';

import 'dart:async';

class LogoWidget extends StatefulWidget {
  @override
  _LogoWidgetState createState() => _LogoWidgetState();
}

class _LogoWidgetState extends State<LogoWidget> with TickerProviderStateMixin {
  AnimationController controller;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(duration: const Duration(milliseconds: 4000), vsync: this);
  }

//  Future<Null> _playAnimation() async {
//    try {
//      await controller.forward().orCancel;
//      await controller.reverse().orCancel;
//    } on TickerCanceled {
//      // the animation got canceled, probably because we were disposed
//    }
//  }

  @override
  Widget build(BuildContext context) {
    controller.forward();
    return LogoStaggerAnimation(controller: controller.view,);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class LogoStaggerAnimation extends StatelessWidget {

  LogoStaggerAnimation({Key key, this.controller}) :

        animationTop = Tween<double>(
          begin: -1.0,
          end: -0.3,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0, 0.500,
              curve: Curves.elasticInOut,
            ),
          ),
        ),

        animationBottom = Tween<double>(
          begin: -1.0,
          end: 0.2,
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.0, 0.500,
              curve: Curves.elasticIn,
            ),
          ),
        ),

        size = Tween<double>(
            begin: 40.0,
            end: 80.0
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(
              0.500, 1.00,
              curve: Curves.bounceIn,
            ),
          ),
        ),

        super(key: key);


  final Animation<double> controller;
  final Animation<double> animationTop;
  final Animation<double> animationBottom;
  final Animation<double> size;


  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: controller,
        builder: (BuildContext context, Widget child) {
          return Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 30.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment(animationTop.value, 0.0),
                        child: Text(
                          'Hello',
                          style: TextStyle(
                              fontSize: 70.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.purple),
                        ),
                      ),
                      Container(
                        alignment: Alignment(animationBottom.value, 0.0),
                        child: Text(
                          'Cook',
                          style: TextStyle(
                              fontSize: size.value,
                              fontWeight: FontWeight.bold,
                              color: Colors.orange),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

}

