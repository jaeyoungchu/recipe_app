import 'package:flutter/material.dart';
import 'package:recipe_app/widgets/signup_logo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class SignUpScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Text('SignUp',style: TextStyle(color: Colors.white,),),
      ),
      body: SingUpBody()
    );
  }
}

class SingUpBody extends StatefulWidget {
  @override
  _SingUpBodyState createState() => _SingUpBodyState();
}


class _SingUpBodyState extends State<SingUpBody> {

  final formKey = new GlobalKey<FormState>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String _email,_name;
  String _password1,_password2;
  bool isLoading;


  @override
  void initState() {
    super.initState();
    isLoading = false;
  }

  _validateForm(){
    final form = formKey.currentState;
    setState(() {
      isLoading =true;
    });
    if(form.validate()){
      form.save();
      if(_password1 == _password2){
        _performLogin();
      }else{
        setState(() {
          isLoading =false;
        });
        final snackBar = new SnackBar(content: Text('Please type same password', ));
        Scaffold.of(context).showSnackBar(snackBar);
      }
    }else{
      setState(() {
        isLoading =false;
      });
    }
  }

  _performLogin() async{

    FirebaseUser user = await _auth.createUserWithEmailAndPassword(email: _email, password: _password1)
        .then((FirebaseUser user)=> _handleAuth(user)).catchError((e){
          _showSnackbar('This email is already used');
          setState(() {
            isLoading=false;
          });
        });
  }

  _handleAuth(FirebaseUser user) async{
    if(user.uid != null){
      SharedPreferences prefs = await SharedPreferences.getInstance().then((prefs){
        prefs.setString('email', user.email);
        //Todo upload user profile with name
        Firestore.instance.collection('userInfo').document(user.email).setData(<String, dynamic>{
          'email': user.email,
          'uid' : user.uid,
          'name' : _name,
          'created_at': FieldValue.serverTimestamp(),
        }).whenComplete((){
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/my_recipe_screen', (Route<dynamic> route) => false);
        });
      });
    } else{
      _showSnackbar('Sign up Error');
    }
    setState(() {
      isLoading = false;
    });
  }

  _showSnackbar(String message){
    final snackBar = new SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 100.0,
                child: SignUpLogo(),),
              SizedBox(
                height: 5.0,
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: TextFormField(
                      validator: (val)=> !val.contains('@') ? 'Invalid Email' : null,
                      onSaved: (val)=> _email = val,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: TextFormField(
                      validator: (val){
                        if(val.length > 18){
                          return 'Too long name';
                        }else if(val.length == 0){
                          return 'Input your name';
                        }else{
                          return null;
                        }
                      } ,
                      onSaved: (val)=> _name = val,
                      decoration: InputDecoration(
                        labelText: 'Name',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: TextFormField(
                      validator: (val)=> val.length >=6 ? null : 'Password too short',
                      onSaved: (val)=> _password1 = val,
                      decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.grey
                          )

                      ),
                      obscureText: true,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: TextFormField(
                      validator: (val)=> val.length >=6 ? null : 'Password too short',
                      onSaved: (val)=> _password2 = val,
                      decoration: InputDecoration(
                          labelText: 'Repeat',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.grey
                          )
                      ),
                      obscureText: true,
                    ),
                  ),
                  SizedBox(
                    height: 60.0,
                  ),
                  Container(
                    height: 50.0,
                    padding: EdgeInsets.symmetric(horizontal: 40.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(20.0),
                      shadowColor: Colors.orange[100],
                      color: Colors.deepOrange,
                      elevation: 7.0,
                      child: GestureDetector(
                        onTap: isLoading ? null :_validateForm,
                        child: Center(
                          child: Text(
                            'SIGN UP',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          !isLoading ?
          SizedBox(height: 0.1,)
              :
          FractionalTranslation(
            translation: Offset(5.0, 6.0),
            child: CircularProgressIndicator(
              backgroundColor: Colors.orange,
              strokeWidth: 6.0,
            ),
          ),
        ],
      ),
    );
  }
}

