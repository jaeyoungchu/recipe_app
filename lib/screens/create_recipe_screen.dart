import 'package:flutter/material.dart';
import 'package:recipe_app/widgets/drawer_widget.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:recipe_app/models/procedure_model.dart';
import 'package:flutter_duration_picker/flutter_duration_picker.dart';
import 'dart:async';
import 'package:recipe_app/models/recipe_detail_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recipe_app/helpers/hashmap_manager.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:recipe_app/models/recipe_simple_model.dart';

class CreateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CreateScreenStateful();
  }
}

class CreateScreenStateful extends StatefulWidget {
  @override
  _CreateScreenState createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreenStateful> {
  File _image;
  List<String> ingredientList;
  List<ProcedureModel> stepList;
  var ingredientController,
      directionController,
      directionEditController,
      foodNameEditController;
  String foodType, foodName;
  GlobalKey<ScaffoldState> _scaffoldKey;
  int editIndex = -1;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool isUploading;
//  Duration _duration;
  //  ScrollController _scrollController, _scrollController2;

  @override
  void initState() {
    super.initState();
    _scaffoldKey = GlobalKey<ScaffoldState>();
    ingredientList = new List();
    stepList = new List();
    ingredientController = TextEditingController();
    directionController = TextEditingController();
    directionEditController = TextEditingController();
    foodNameEditController = TextEditingController();
    isUploading = false;
//    _scrollController = new ScrollController(
//      initialScrollOffset: 0.0,
//      keepScrollOffset: true,
//    );
//    _scrollController2 = new ScrollController(
//      initialScrollOffset: 0.0,
//      keepScrollOffset: true,
//    );
//    _duration = Duration(minutes: 0, seconds: 0);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(
            'Create Recipe',
            style: TextStyle(color: Colors.white),
          ),
          iconTheme: IconThemeData(color: Colors.white),
        ),
        drawer: DrawerWidget(),
        body: Stack(
          children: <Widget>[

            Container(
              color: Colors.blueGrey[50],
              child: ListView(
                children: <Widget>[
                  _getCameraCard(),
                  _getRadioCard(),
                  _getTitleCard(),
                  SizedBox(
                    height: 10.0,
                  ),
                  _getIngredientCard(),
                  SizedBox(
                    height: 10.0,
                  ),
                  _getDirectionCard1(),
                  SizedBox(
                    height: 10.0,
                  ),
                  _getSubmitCard(),
                ],
              ),
            ),
            !isUploading
                ? SizedBox(
              height: 0.1,
            )
                : FractionalTranslation(
              translation: Offset(4.5, 6.0),
              child: CircularProgressIndicator(
                backgroundColor: Colors.orange,
                strokeWidth: 6.0,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _saveToServer() {
    String errMessage = '';
    if (foodType == null) {
      errMessage = 'Please input your food type';
    }
    if (foodName == null) {
      errMessage = 'Please input your recipe name';
    }
    if (_image == null) {
      errMessage = 'Please select your food image';
    }
    if (ingredientList.length == 0) {
      errMessage = 'Please input ingredients';
    }
    if (stepList.length == 0) {
      errMessage = 'Please input directions';
    }

    if (errMessage.isEmpty) {
      setState(() {
        isUploading = true;
      });
      createRecipeDetail();
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        errMessage,
      )));
    }
  }

  createRecipeDetail() async {
    await _auth.currentUser().then((user) {
      Firestore.instance
          .collection('userInfo')
          .document(user.email)
          .get()
          .then((documentsnapshot) {
        if (documentsnapshot.exists) {
          String token =
              '${user.email}${DateTime.now().millisecondsSinceEpoch}';
          String fileName =
              '${user.email}${DateTime.now().millisecondsSinceEpoch}.jpg';
          final StorageReference firebaseStorageRef = FirebaseStorage.instance
              .ref()
              .child('images')
              .child('${user.email}')
              .child(fileName);

          firebaseStorageRef.putFile(_image).onComplete.then((val) {
            val.ref.getDownloadURL().then((url) {
              RecipeDetail recipeDetail = RecipeDetail(
                  foodName,
                  documentsnapshot.data['name'],
                  url,
                  foodType,
                  token,
                  user.email,
                  0,
                  0,
                  ingredientList,
                  stepList);
              Map<String, dynamic> map =
                  HashMapManager().createRecipeDetailDocument(recipeDetail);
              Firestore.instance
                  .collection('recipeDetails')
                  .document(recipeDetail.token)
                  .setData(map)
                  .whenComplete(() {
                RecipeSimpleModel simpleModel = RecipeSimpleModel(foodName, foodType, documentsnapshot.data['name'], url, token,user.email, 0, 0);
                Map<String, dynamic> simpleMap = HashMapManager().createSimpleRecipeDetailMap(simpleModel);

                Firestore.instance.collection('simpleRecipes').document(token).setData(simpleMap).whenComplete((){
                  setState(() {
                    isUploading = false;
                  });
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/my_recipe_screen', (Route<dynamic> route) => false);
                }).catchError((e){
                  print(e);
                  setState(() {
                    isUploading = false;
                  });
                });

              });
            });
          });
        } else {
          setState(() {
            isUploading = false;
          });
          _scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(
            'Server Error',
          )));
        }
      }).catchError((e) {
        print(e);
        setState(() {
          isUploading = false;
        });
        return null;
      });
    }).catchError((e) {
      print(e);
      setState(() {
        isUploading = false;
      });
      return null;
    });
  }

  _setEditIndex(int index) {
    setState(() {
      editIndex = index;
      directionEditController.text = stepList[index].detail;
    });
  }

  _removeIngredient(int index) {
    setState(() {
      ingredientList.removeAt(index);
    });
  }

  _removeDirection(int index) {
    setState(() {
      stepList.removeAt(index);
      stepList.removeWhere((value) => value == null);
      editIndex = -1;
    });
  }

  _addIngredient() {
    if (ingredientController.text.toString().length == 0) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Please type ingredient',
      )));
      return;
    }
    if (ingredientController.text.toString().length >= 40) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Too long ingredient name',
      )));
      return;
    }
    setState(() {
      ingredientList.add(ingredientController.text);
      ingredientController.text = '';
    });
//    _scrollController.animateTo(
//      _scrollController.position.maxScrollExtent + 100.0,
//      duration: const Duration(milliseconds: 500),
//      curve: Curves.ease,
//    );
  }

  _setDurationResult(Duration _result, int index) {
    if (_result.inMinutes == null) {
      _result = Duration(minutes: 0);
    }
    if (_result.inMinutes > 999) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Too long time',
      )));
      return;
    }
    setState(() {
      stepList[index].alarm = _result.inMinutes;
    });
  }

  _addStep() {
    if (directionController.text.toString().length == 0) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Please type details',
      )));
      return;
    }
    if (directionController.text.toString().length >= 300) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Too long details',
      )));
      return;
    }
    setState(() {
      stepList.add(new ProcedureModel(0, directionController.text));
      directionController.text = '';
//      _duration = Duration(minutes: 0);
    });
//    _scrollController2.animateTo(
//      _scrollController2.position.maxScrollExtent + 100.0,
//      duration: const Duration(milliseconds: 500),
//      curve: Curves.ease,
//    );
  }

  _editStep(int index) {
    if (directionEditController.text.toString().length == 0) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Please type Direction',
      )));
      return;
    }
    if (directionEditController.text.toString().length >= 300) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(
        'Too long Direction',
      )));
      return;
    }
    setState(() {
      stepList[index].detail = directionEditController.text;
      directionEditController.text = '';
      editIndex = -1;
    });
  }

  Future _getImageFromGalley() async {

    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
//    ImageProperties imageProperties = await FlutterNativeImage.getImageProperties(image.path);
//    print('width : ${imageProperties.width}');
//    print('height : ${imageProperties.height}');
//    if(imageProperties.width < imageProperties.height){
//      _scaffoldKey.currentState.showSnackBar(SnackBar(
//          content: Text(
//            'Please select landscape image',
//          )));
//      return;
//    }
//    image = await FlutterNativeImage.cropImage(image.path, 0, 0, 1280, 720);
    image = await FlutterNativeImage.compressImage(image.path,
        quality: 95, targetWidth: 1280, targetHeight: 720);

//    var size = await image.length();
//    print('file size : $size');
//    bool flag = true;
//    do{
//      var size = await image.length();
//      print('file size 1: ${size}');
//      if(size < 1234567){
//        flag = false;
//      }
//      image = await FlutterNativeImage.compressImage(image.path,quality: 90,percentage: 90);
//      print('file size 2: ${size}');
//    }while(flag);
//    print('file size 3:');
    setState(() {
      _image = image;
    });
  }

  Future _getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  _onRadioButtonTap(String type) {
    setState(() {
      foodType = type;
    });
  }

  Widget _getRadioCard() {
    return Container(
      height: 50.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          RadioButtonCustom('Western',
              foodType == 'Western' ? Colors.orange : Colors.grey[400], () {
            _onRadioButtonTap('Western');
          }),
          RadioButtonCustom('Oriental',
              foodType == 'Oriental' ? Colors.orange : Colors.grey[400], () {
            _onRadioButtonTap('Oriental');
          }),
          RadioButtonCustom(
              'Fusion', foodType == 'Fusion' ? Colors.orange : Colors.grey[400],
              () {
            _onRadioButtonTap('Fusion');
          }),
        ],
      ),
    );
  }

  Widget _getTitleCard() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          alignment: Alignment(-0.9, 0.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white10),
            color: Colors.orange,
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: Text(
            foodName == null ? 'Title:' : '${foodName}',
            textAlign: TextAlign.left,
            style: foodName == null
                ? TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  )
                : TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0),
          ),
        ),
        SizedBox(
          height: 4.0,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white10),
            color: Colors.white,
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 6.0),
                  child: TextField(
                    controller: foodNameEditController,
                    decoration: InputDecoration(
                      labelText: 'Title',
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 14.0),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: IconButton(
                  icon: Icon(
                    Icons.add_circle_outline,
                    size: 28.0,
                    color: Colors.orange,
                  ),
                  onPressed: isUploading ? null : () {
                    setState(() {
                      foodName = foodNameEditController.text;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _getIngredientCard() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          alignment: Alignment(-0.9, 0.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white10),
            color: Colors.orange,
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: Text(
            'Ingredients:',
            textAlign: TextAlign.left,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          height: 4.0,
        ),
        Column(
          children: _getIngredientListView(),
        ),
        SizedBox(
          height: 4.0,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white10),
            color: Colors.white,
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 9,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 6.0),
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Ingredient',
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          fontSize: 14.0),
                    ),
                    controller: ingredientController,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: IconButton(
                  icon: Icon(
                    Icons.add_circle_outline,
                    size: 28.0,
                    color: Colors.orange,
                  ),
                  onPressed: isUploading ? null : _addIngredient,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> _getIngredientListView() {
    return List.generate(ingredientList.length, (index) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 3.0),
        margin: EdgeInsets.fromLTRB(10.0, 4.0, 10.0, 0.0),
        alignment: Alignment(-1.0, 0.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white10),
          color: Colors.white,
          borderRadius: BorderRadius.circular(2.0),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Icon(
                  Icons.add_circle,
                  color: Colors.blueGrey,
                )),
            Expanded(
              flex: 8,
              child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    ingredientList[index],
                    style: TextStyle(
                        color: Colors.blueGrey, fontWeight: FontWeight.bold),
                  )),
            ),
            Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () => isUploading ? null : _removeIngredient(index),
                  child: Icon(
                    Icons.cancel,
                    color: Colors.grey,
                  ),
                )),
          ],
        ),
      );
    });
  }

  Widget _getCameraCard() {
    return Card(
      child: Container(
        height: 180.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: Card(
                  color: Colors.grey,
                  child: _image == null
                      ? Icon(
                          Icons.camera,
                          size: 100.0,
                          color: Colors.black38,
                        )
                      : Image.file(
                          _image,
                          fit: BoxFit.fill,
                        )),
            ),
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: isUploading ? null : _getImageFromCamera,
                      child: Card(
                        margin: EdgeInsets.fromLTRB(2.0, 5.0, 5.0, 5.0),
                        color: Colors.orange[500],
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.camera_alt,
                              size: 40.0,
                              color: Colors.white,
                            ),
                            Text(
                              'Camera',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: isUploading ? null : _getImageFromGalley,
                      child: Card(
                        margin: EdgeInsets.fromLTRB(2.0, 5.0, 5.0, 5.0),
                        color: Colors.orange[500],
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.account_box,
                              size: 40.0,
                              color: Colors.white,
                            ),
                            Text(
                              'Gallery',
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getDirectionCard1() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          margin: EdgeInsets.symmetric(horizontal: 10.0),
          alignment: Alignment(-0.9, 0.0),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white10),
            color: Colors.orange,
            borderRadius: BorderRadius.circular(2.0),
          ),
          child: Text(
            'Directions:',
            textAlign: TextAlign.left,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          height: 4.0,
        ),
        Column(
          children: _getDirectionListView(),
        ),
        SizedBox(
          height: 4.0,
        ),
        editIndex != -1
            ? Container()
            : Container(
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white10),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(2.0),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 7,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 6.0),
                        child: TextField(
                          maxLines: 3,
                          decoration: InputDecoration(
                            labelText: 'Direction',
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.grey,
                                fontSize: 14.0),
                          ),
                          controller: directionController,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: IconButton(
                        icon: Icon(
                          Icons.add_circle_outline,
                          size: 28.0,
                          color: Colors.orange,
                        ),
                        onPressed: isUploading ? null : _addStep,
                      ),
                    ),
                  ],
                ),
              ),
      ],
    );
  }

  List<Widget> _getDirectionListView() {
    return List.generate(stepList.length, (index) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 3.0),
        margin: EdgeInsets.fromLTRB(10.0, 4.0, 10.0, 0.0),
        alignment: Alignment(-1.0, 0.0),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white10),
          color: Colors.white,
          borderRadius: BorderRadius.circular(2.0),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 20.0,
              height: 20.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25.0),
                color: Colors.blueGrey[400],
              ),
              child: Center(
                  child: Text(
                '${index + 1}',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 10.0),
              )),
            ),
            editIndex != index
                ? Expanded(
                    flex: 75,
                    child: GestureDetector(
                      onTap: () => isUploading ? null : _setEditIndex(index),
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          stepList[index].detail,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontWeight: FontWeight.bold),
                          maxLines: 5,
                        ),
                      ),
                    ),
                  )
                : Expanded(
                    flex: 75,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: TextField(
                        maxLines: 3,
                        decoration: InputDecoration(
                          labelText: 'Edit',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.grey,
                              fontSize: 14.0),
                        ),
                        controller: directionEditController,
                      ),
                    ),
                  ),
            GestureDetector(
              onTap: isUploading
                  ? null
                  : () async {
                      var _result = await showDurationPicker(
                        context: context,
                        initialTime: new Duration(minutes: 0),
                        snapToMins: 1.0,
                      );
                      if(_result == null){
                        _result = Duration(minutes: 0);
                      }
                      _setDurationResult(_result, index);
                    },
              //stepList[index].alarm <2 ? Text('${stepList[index].alarm}min'):Text('${stepList[index].alarm}mins'),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${stepList[index].alarm}',
                    style: TextStyle(color: Colors.blueGrey, fontSize: 18.0),
                  ),
                  stepList[index].alarm < 2
                      ? Text(
                          'min',
                          style:
                              TextStyle(color: Colors.blueGrey, fontSize: 8.0),
                        )
                      : Text(
                          'mins',
                          style:
                              TextStyle(color: Colors.blueGrey, fontSize: 8.0),
                        ),
                ],
              ),
            ),
            editIndex == index
                ? Expanded(
                    flex: 10,
                    child: GestureDetector(
                      onTap: () => isUploading ? null : _editStep(index),
                      child: Icon(
                        Icons.add_circle_outline,
                        color: Colors.orange,
                      ),
                    ))
                : Expanded(
                    flex: 10,
                    child: GestureDetector(
                      onTap: () => isUploading ? null : _showConfirmDialog(index),
                      child: Icon(
                        Icons.cancel,
                        color: Colors.grey,
                      ),
                    )),
          ],
        ),
      );
    });
  }

  Widget _getSubmitCard() {
    return Container(
      height: 50.0,
      padding: EdgeInsets.symmetric(horizontal: 40.0),
      child: Material(
        borderRadius: BorderRadius.circular(20.0),
        shadowColor: Colors.orange[100],
        color: Colors.deepOrange,
        elevation: 7.0,
        child: GestureDetector(
          onTap: isUploading ? null : _saveToServer,
          child: Center(
            child: Text(
              'SAVE',
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0),
            ),
          ),
        ),
      ),
    );
  }

  _showConfirmDialog(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(
            "Do you want to remove this direction?",
            style: TextStyle(color: Colors.blueGrey, fontSize: 18.0),
          ),
          actions: <Widget>[
            Row(
              children: <Widget>[
                new FlatButton(
                  child: new Text("Cancel",style: TextStyle(color: Colors.blueGrey),),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                new FlatButton(
                  child: new Text("Yes",style: TextStyle(color: Colors.blueGrey),),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _removeDirection(index);
                  },
                ),
              ],
            ),
            // usually buttons at the bottom of the dialog
          ],
        );
      },
    );
  }


}

class RadioButtonCustom extends StatelessWidget {
  final String title;
  final Color bgColor;
  final VoidCallback _onTap;

  RadioButtonCustom(this.title, this.bgColor, this._onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: GestureDetector(
        onTap: _onTap,
        child: Container(
          width: 100.0,
          decoration: BoxDecoration(
              border: Border.all(color: bgColor),
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.white),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(color: bgColor, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
