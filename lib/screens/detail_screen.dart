import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:recipe_app/models/recipe_detail_model.dart';
import 'package:recipe_app/models/recipe_simple_model.dart';
import 'package:recipe_app/widgets/custom_timer_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recipe_app/helpers/hashmap_manager.dart';
import 'package:firebase_auth/firebase_auth.dart';

class DetailScreen extends StatefulWidget {
  final String token;

  DetailScreen(this.token);

  @override
  _DetailScreenState createState() => _DetailScreenState(token);
}

class _DetailScreenState extends State<DetailScreen> {
  int _currentIndex = 0;
  int likeCount=0;
  PageController _pageController;
  String token;
  FirebaseUser user;
  RecipeDetail recipeDetail;
  bool isFavorite,alreadyLike,isMyRecipe,isChanged;
  _DetailScreenState(this.token);
  GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
    _scaffoldKey = GlobalKey<ScaffoldState>();
    isFavorite = false;
    alreadyLike = false;
    isMyRecipe = false;
    isChanged = false;
    _getModel();
  }

  void onPageChanged(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: true,
      bottomNavigationBar: BottomNavigationBar(
        onTap: navigationTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.domain),
            title: new Text('Ingredients'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.domain),
            title: new Text('Directions'),
          ),
        ],
      ),
      body: recipeDetail == null ? FractionalTranslation(
        translation: Offset(5.0, 8.0),
        child: CircularProgressIndicator(
          backgroundColor: Colors.orange,
          strokeWidth: 6.0,
        ),
      )
          :
      PageView(
        children: <Widget>[
          _getIngredientsTab(),
          _getDirectionTab(),
        ],
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
    );
  }

  Widget _getSliverAppBar(){
    return SliverAppBar(
      iconTheme: IconThemeData(color: Colors.white),
      title: Text(
        recipeDetail.recipeName,
        style:
        TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      actions: <Widget>[
        isMyRecipe ? GestureDetector(
          onTap: _showConfirmDialog,
          child: Row(
            children: <Widget>[
              Icon(
                Icons.delete,
                size: 25.0,
              ),
              SizedBox(
                width: 18.0,
              ),
            ],
          ),
        )
            :
        Container(),
        GestureDetector(
          onTap: _tappedLike,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.thumb_up,
                color: alreadyLike ? Colors.red : Colors.white,
                size: 20.0,
              ),
              SizedBox(
                width: 8.0,
              ),
              Text(
                '$likeCount',
                style: TextStyle(
                    fontSize: 20.0,
                    color: alreadyLike ? Colors.red : Colors.white,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(width: 30.0,),
        GestureDetector(
          onTap: _tappedFavorite,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.favorite_border,
                color: isFavorite ? Colors.red : Colors.white,
                size: 24.0,
              ),
              SizedBox(width: 4.0,),
            ],
          ),
        ),
      ],
//            floating: true,
      pinned: true,
      expandedHeight: 160.0,
//              flexibleSpace: FlexibleSpaceBar(background: Image.network('https://placeimg.com/480/320/any',fit: BoxFit.fill,),),
      flexibleSpace: FlexibleSpaceBar(
        background: CachedNetworkImage(
          placeholder: Center(child: CircularProgressIndicator()),
          imageUrl: recipeDetail.imageUrl,
          fit: BoxFit.fill,
        ),
      ),
    );
  }



  Widget _getIngredientsTab() {

    return SafeArea(
      child: CustomScrollView(
        slivers: <Widget>[
          _getSliverAppBar(),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              mainAxisSpacing: 0.1,
              crossAxisSpacing: 1.0,
              childAspectRatio: 10.0,
            ),
            delegate: SliverChildBuilderDelegate(
              (context, i) => Card(
                    child: Container(
                      padding: EdgeInsets.all(2.0),
                      color: Colors.orange[600],
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.add_circle_outline,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(recipeDetail.ingredientList[i],
                                  style: TextStyle(
                                      fontFamily: 'Mail',
                                      color: Colors.white,
                                      fontSize: 17.0)),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
              childCount: recipeDetail.ingredientList.length,
            ),
          ),
        ],
      ),
    );
  }



  Widget _getDirectionTab() {
    return SafeArea(
      child: CustomScrollView(
        slivers: <Widget>[
          _getSliverAppBar(),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              mainAxisSpacing: 0.1,
              crossAxisSpacing: 1.0,
              childAspectRatio: 2.5,
            ),
            delegate: SliverChildBuilderDelegate(
              (context, i) => Card(
                    child: Container(
                      padding: EdgeInsets.all(3.0),
                      color: Colors.orange[600],
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '${i + 1}.',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Mail'),
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Expanded(
                                  child:
                                      Text(recipeDetail.directionList[i].detail,
                                          style: TextStyle(
                                            fontFamily: 'Mail',
                                            color: Colors.white,
                                          ))),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    0.0, 1.0, 0.0, 0.0),
                                child: Column(
                                  children: <Widget>[
//                              SizedBox(height: 40.0,),
                                    recipeDetail.directionList[i].alarm == 0
                                        ? SizedBox(
                                            width: 1.0,
                                          )
                                        : CustomTimeWidget(
                                            10.0,
                                            40.0,
                                            recipeDetail
                                                .directionList[i].alarm),
//                                recipeDetail.directionList[i].alarm == 0 ?
//                                  Text('${recipeDetail.directionList[i].alarm} min',style: TextStyle(color: Colors.orange[500],fontWeight: FontWeight.bold,fontFamily: 'Mail'),)
//                                      :
//                                  Text('${recipeDetail.directionList[i].alarm} mins',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontFamily: 'Mail'),)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
              childCount: recipeDetail.directionList.length,
            ),
          ),
        ],
      ),
    );
  }

  _tappedFavorite(){
    if(isMyRecipe){
      return;
    }
    setState(() {
      isChanged =true;
    });
    CollectionReference collectionReference = Firestore.instance.collection('favorites').document(recipeDetail.token).collection('follower');
    CollectionReference colRef = Firestore.instance.collection('myFavorites').document(user.email).collection('following');

    if(isFavorite){
      collectionReference.document(user.email).delete();
      colRef.document(recipeDetail.token).delete();
    }else{
      collectionReference.document(user.email).setData({'like':'yes'});
      RecipeSimpleModel simpleModel =
      RecipeSimpleModel(recipeDetail.recipeName, recipeDetail.recipeType, recipeDetail.author, recipeDetail.imageUrl, token,recipeDetail.authEmail, likeCount, recipeDetail.favoriteCount);
      Map<String, dynamic> simpleMap = HashMapManager().createSimpleRecipeDetailMap(simpleModel);
      colRef.document(recipeDetail.token).setData(simpleMap);
    }

    _getFavoriteData();
  }

  _getFavoriteData()async{
    if(recipeDetail == null){
      return;
    }
    CollectionReference collectionReference = Firestore.instance.collection('favorites').document(recipeDetail.token).collection('follower');

    collectionReference.document(user.email).get().then((documentSnapshot){
      if(documentSnapshot.exists){
        setState(() {
          isFavorite = true;
        });
      }else{
        setState(() {
          isFavorite = false;
        });
      }
    });
  }


  _tappedDelete() async{
    if(recipeDetail == null){
      return;
    }
    await Firestore.instance.collection('simpleRecipes').document(token).delete();
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          'Server Error',
        )));
    Navigator.of(context).pushNamedAndRemoveUntil(
        '/my_recipe_screen', (Route<dynamic> route) => false);

  }

  _tappedLike(){
    if(isMyRecipe){
      return;
    }
    setState(() {
      isChanged =true;
    });
    CollectionReference collectionReference = Firestore.instance.collection('likeFollower').document(recipeDetail.token).collection('follower');

    if(alreadyLike){
      collectionReference.document(user.email).delete();
    }else{
      collectionReference.document(user.email).setData({'like':'yes'});
    }
    _getLikeCount();
  }

  _getLikeCount() async{
    if(recipeDetail == null){
      return;
    }

    CollectionReference collectionReference = Firestore.instance.collection('likeFollower').document(recipeDetail.token).collection('follower');
    QuerySnapshot querySnapshot = await collectionReference.getDocuments();

    if(recipeDetail.authEmail == user.email){
      setState(() {
        isMyRecipe = true;
      });
    }

    collectionReference.document(user.email).get().then((documentSnapshot){
      if(documentSnapshot.exists){
        setState(() {
          alreadyLike = true;
          likeCount = querySnapshot.documents.length;
        });
      }else{
        setState(() {
          alreadyLike = false;
          likeCount = querySnapshot.documents.length;
        });
      }
    });
  }
  _getModel() async {
    user = await FirebaseAuth.instance.currentUser();
    RecipeDetail detailModel;

    QuerySnapshot querySnapshot = await Firestore.instance.collection('recipeDetails').where('token',isEqualTo: token).getDocuments();
    querySnapshot.documents.forEach((document){
      detailModel = HashMapManager().getDetailModelFromDocument(document);
    });

    setState(() {
      recipeDetail = detailModel;
    });

    _getLikeCount();
    _getFavoriteData();

  }

  @override
  void dispose() {
    super.dispose();
    _updateLikeFavorite();
  }

  _updateLikeFavorite()async{
    if(recipeDetail == null){
      return;
    }

    if(isChanged){
      DocumentSnapshot documentSnapshot = await Firestore.instance.collection('simpleRecipes').document(token).get();

      if(documentSnapshot != null && documentSnapshot.exists){

        CollectionReference collectionReference = Firestore.instance.collection('likeFollower').document(recipeDetail.token).collection('follower');
        QuerySnapshot querySnapshot = await collectionReference.getDocuments();
        int likeCount = querySnapshot.documents.length;
        print(likeCount);
        Firestore.instance.collection('simpleRecipes').document(token).updateData({'like':likeCount});
      }

      documentSnapshot = await Firestore.instance.collection('myFavorites').document(user.email).collection('following').document(recipeDetail.token).get();

      if(documentSnapshot != null && documentSnapshot.exists){
        Firestore.instance.collection('myFavorites').document(user.email).collection('following').document(recipeDetail.token).updateData({'like':likeCount});
      }

    }
  }

  void _showConfirmDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(
            "Do you want to delete your recipe?",
            style: TextStyle(color: Colors.blueGrey, fontSize: 18.0),
          ),
          actions: <Widget>[
            Row(
              children: <Widget>[
                new FlatButton(
                  child: new Text("Cancel",style: TextStyle(color: Colors.blueGrey),),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                new FlatButton(
                  child: new Text("Yes",style: TextStyle(color: Colors.blueGrey),),
                  onPressed: () {
                    Navigator.of(context).pop();
                    _tappedDelete();
                  },
                ),
              ],
            ),
            // usually buttons at the bottom of the dialog
          ],
        );
      },
    );
  }

}
