import 'package:flutter/material.dart';
import 'package:recipe_app/widgets/logo_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      body: LoginStateful(),
    );
  }
}

class LoginStateful extends StatefulWidget {
  @override
  _LoginStatefulState createState() => _LoginStatefulState();
}

class _LoginStatefulState extends State<LoginStateful> {
  final formKey = new GlobalKey<FormState>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
//  final GoogleSignIn googleSignIn = new GoogleSignIn();
  bool preventClick;
  SharedPreferences prefs;
  String _email;
  String _password;
  TextEditingController emailController, resetEmailController;

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  @override
  void initState() {
    print('initsate');
    emailController = new TextEditingController();
    _setEmailFromPref();
    preventClick = false;
    super.initState();
  }

  Future<FirebaseUser> _signInGoogle() async {
//    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
//    GoogleSignInAccount  _currentUser;
//    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
//        _currentUser = account;
//      if (_currentUser != null) {
////        GoogleSignInAuthentication gSA = await _currentUser.authentication;
//      print('_currentUser not null');
//      }
//    });
//    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAccount googleSignInAccount = _googleSignIn.currentUser;
    if (googleSignInAccount == null)
      googleSignInAccount = await _googleSignIn.signInSilently();
    if (googleSignInAccount == null) {
      googleSignInAccount = await _googleSignIn.signIn();
    }

//    var response = await _auth.currentUser();
//    if (response.uid == null) {
//      GoogleSignInAuthentication credentials = await _googleSignIn.currentUser.authentication;
//      FirebaseUser user = await _auth.signInWithGoogle(
//        idToken: credentials.idToken, accessToken: credentials.accessToken,
//      );
//    }

    GoogleSignInAuthentication gSA = await googleSignInAccount.authentication;

    FirebaseUser user = await _auth.signInWithGoogle(
        idToken: gSA.idToken, accessToken: gSA.accessToken);

    return user;
  }

  Future<FirebaseUser> _signInEmail() async {
    FirebaseUser user = await _auth.signInWithEmailAndPassword(
        email: _email, password: _password);
    return user;
  }

  _setEmailFromPref() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      emailController.text =
          prefs.getString('email') == null ? "" : prefs.getString('email');
    });
  }

  _validateForm() {
    setState(() {
      preventClick = true;
    });

    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      _emailLogin();
    } else {
      setState(() {
        preventClick = false;
      });
    }
  }

  _emailLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    _signInEmail().then((FirebaseUser user) {
      if (user.uid != null) {
        prefs.setString('email', user.email);
        _password = '';
        Navigator.of(context).pushNamedAndRemoveUntil(
            '/my_recipe_screen', (Route<dynamic> route) => false);
      }
    }).catchError((e) {
      setState(() {
        preventClick = false;
      });
      _showSnackBar('Wrong password or id');
    });
  }

  _googleLogin() {
    setState(() {
      preventClick = true;
    });
    _signInGoogle().then((FirebaseUser user) {
      if (user.uid != null) {
        Firestore.instance
            .collection('userInfo')
            .where('email', isEqualTo: user.email)
            .getDocuments()
            .then((QuerySnapshot snapshot) {
          print(snapshot.toString());
        });
        Firestore.instance
            .collection('userInfo')
            .document(user.email)
            .get()
            .then((datasnapshot) {
          if (!datasnapshot.exists) {
            Firestore.instance
                .collection('userInfo')
                .document(user.email)
                .setData(<String, dynamic>{
              'email': user.email,
              'uid': user.uid,
              'name': user.displayName,
              'created_at': FieldValue.serverTimestamp(),
            });
          }
        });

        Navigator.of(context).pushNamedAndRemoveUntil(
            '/my_recipe_screen', (Route<dynamic> route) => false);
      }
    }).catchError((e) {
      _showSnackBar(e.toString());
      setState(() {
        preventClick = false;
      });
    });
  }

  _showSnackBar(String message) {
    final snackBar = new SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar((snackBar));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: new LinearGradient(
          // new
          // Where the linear gradient begins and ends
          begin: Alignment.topRight, // new
          end: Alignment.bottomCenter, // new
          // Add one stop for each color.
          // Stops should increase
          // from 0 to 1
          stops: [0.1, 0.5, 0.7, 0.9],
          colors: [
            // Colors are easy thanks to Flutter's
            // Colors class.
//            Colors.orange[800],
//            Colors.orange[700],
//            Colors.orange[500],
//            Colors.orange[400],
            Colors.white,
            Colors.white,
            Colors.white,
            Colors.white,
          ],
        ),
      ),
      child: Form(
        key: formKey,
        child: ListView(
          children: <Widget>[
            SizedBox(height: 220.0, child: LogoWidget()),
            Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: TextFormField(
                        validator: (val) =>
                            !val.contains('@') ? 'Invalid Email' : null,
                        onSaved: (val) => _email = val,
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0),
                      child: TextFormField(
                        validator: (val) =>
                            val.length >= 6 ? null : 'Password too short',
                        onSaved: (val) => _password = val,
                        decoration: InputDecoration(
                            labelText: 'Password',
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.grey)),
                        obscureText: true,
                      ),
                    ),
                    !preventClick
                        ? SizedBox(
                            height: 0.1,
                          )
                        : FractionalTranslation(
                            translation: Offset(0.0, -2.0),
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.orange,
                              strokeWidth: 6.0,
                            ),
                          ),
                  ],
                ),
              ],
            ),
            Container(
              alignment: Alignment(1.0, 0.0),
              padding: EdgeInsets.fromLTRB(0.0, 15.0, 40.0, 0.0),
              child: InkWell(
                onTap: preventClick ? null : _showDialog,
                child: Text(
                  'Forgot Password',
                  style: TextStyle(
                    color: Colors.orangeAccent,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              height: 50.0,
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              child: Material(
                borderRadius: BorderRadius.circular(20.0),
                shadowColor: Colors.orange[100],
                color: Colors.deepOrange,
                elevation: 7.0,
                child: GestureDetector(
                  onTap: preventClick ? null : _validateForm,
                  child: Center(
                    child: Text(
                      'LOGIN',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
              height: 50.0,
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                        color: Colors.deepOrangeAccent,
                        style: BorderStyle.solid,
                        width: 1.0),
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(20.0)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Image(
                          image: AssetImage('images/google_login_icon.png'),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 2.0,
                    ),
                    GestureDetector(
                      onTap: preventClick ? null : _googleLogin,
                      child: Center(
                        child: Text(
                          'Login with google',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.orange,
                              fontSize: 18.0),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              alignment: Alignment(-1.0, 0.0),
              padding: EdgeInsets.fromLTRB(40.0, 25.0, 0.0, 0.0),
              child: InkWell(
                onTap: () {
                  preventClick
                      ? null
                      : Navigator.of(context).pushNamed('/signup_screen');
                },
                child: Text(
                  'Create new account',
                  style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
//                  decoration: TextDecoration.underline,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showDialog() {
    resetEmailController = new TextEditingController();
    resetEmailController.text =
        prefs.getString('email') == null ? "" : prefs.getString('email');
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(
            "Input your e-mail address",
            style: TextStyle(color: Colors.orange, fontSize: 18.0),
          ),
          content: TextField(
            controller: resetEmailController,
            decoration: InputDecoration(
              labelText: 'Email',
              labelStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.grey),
            ),
          ),
          actions: <Widget>[
            Row(
              children: <Widget>[
                new FlatButton(
                  child: new Text("Cancel"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                new FlatButton(
                  child: new Text("Send"),
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (resetEmailController.text.length == 0) {
                      _showSnackBar('Please input email');
                    } else {
                      _auth.sendPasswordResetEmail(
                          email: resetEmailController.text);
                      _showSnackBar(
                          'Please reset your password in ${resetEmailController.text}');
                    }
                  },
                ),
              ],
            ),
            // usually buttons at the bottom of the dialog
          ],
        );
      },
    );
  }
}
