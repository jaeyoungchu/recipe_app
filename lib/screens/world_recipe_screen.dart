import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:recipe_app/models/recipe_simple_model.dart';
import 'package:recipe_app/widgets/drawer_widget.dart';
import 'detail_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recipe_app/helpers/hashmap_manager.dart';

class WorldRecipeScreen extends StatefulWidget {
  @override
  _WorldRecipeScreenState createState() => _WorldRecipeScreenState();
}

class _WorldRecipeScreenState extends State<WorldRecipeScreen> {
  int _currentIndex=0;
  PageController _pageController;
  bool isSortByLike;

  void onPageChanged(int index) {
    setState(() {
      _currentIndex = index;
      simpleList = List();
    });
    _getModel(index);
  }

  void navigationTapped(int page) {
    // Animating to the page.
    // You can use whatever duration and curve you like
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }
  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
    simpleList = List();
    isSortByLike = false;
    _getModel(0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: navigationTapped,
        currentIndex: _currentIndex, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.domain),
            title: new Text('Western'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.domain),
            title: new Text('Oriental'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.domain),
            title: new Text('Fusion'),
          ),
        ],
      ),
      drawer: DrawerWidget(),
      body: PageView(
        children: <Widget>[
          _getTapScreen(0),
          _getTapScreen(1),
          _getTapScreen(2),
        ],
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
    );
  }

  _moveToDetailScreen(String token){
    Navigator.push(context, MaterialPageRoute(builder: (_){
      return DetailScreen(token);
    }));
  }


  List<RecipeSimpleModel> simpleList;

  _getTapScreen(int index) {

    String title,appbarUrl;
    if(index == 0){
      title = 'Western';
      appbarUrl = 'images/western_appbar.jpg';
    }else if(index == 1){
      title = 'Oriental';
      appbarUrl = 'images/oriental_appbar.jpg';
    }else if(index == 2){
      title = 'Fusion';
      appbarUrl = 'images/fusion_appbar.jpg';
    }

    return SafeArea(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            actions: <Widget>[
              GestureDetector(
                onTap: _tappedSort,
                child: Row(
                  children: <Widget>[
                    isSortByLike ?
                    Row(
                      children: <Widget>[
                        Icon(Icons.arrow_upward, color: Colors.white,size: 22.0,),
                        Text('Like',style: TextStyle(color: Colors.white,fontSize: 22.0,fontWeight: FontWeight.bold),)
                      ],
                    )
                        :
                    Row(
                      children: <Widget>[
                        Icon(Icons.arrow_upward, color: Colors.white,size: 12.0,),
                        Text('Like',style: TextStyle(color: Colors.white,fontSize: 12.0),)
                      ],
                    ),
                    SizedBox(width: 10.0,),
                    isSortByLike ?
                    Row(
                      children: <Widget>[
                        Icon(Icons.arrow_upward, color: Colors.white,size: 12.0,),
                        Text('New',style: TextStyle(color: Colors.white,fontSize: 12.0),)
                      ],
                    )
                        :
                    Row(
                      children: <Widget>[
                        Icon(Icons.arrow_upward, color: Colors.white,size: 22.0,),
                        Text('New',style: TextStyle(color: Colors.white,fontSize: 22.0,fontWeight: FontWeight.bold),)
                      ],
                    ),
                    SizedBox(width: 20.0,)
                  ],
                ),
              )

            ],
            iconTheme: IconThemeData(color: Colors.white),
            title: Text(
              title,
              style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
            ),
//            floating: true,
            pinned: true,
            expandedHeight: 160.0,
//              flexibleSpace: FlexibleSpaceBar(background: Image.network('https://placeimg.com/480/320/any',fit: BoxFit.fill,),),
            flexibleSpace: FlexibleSpaceBar(
              background: Image.asset(
                appbarUrl,
                fit: BoxFit.fill,

              ),
            ),
          ),
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 0.1,
              crossAxisSpacing: 0.1,
              childAspectRatio: 0.78,
            ),
            delegate: SliverChildBuilderDelegate(
                  (context, i) => Card(
                child: GestureDetector(
                  onTap: ()=>_moveToDetailScreen(simpleList[i].token),
                  child: Container(
                    padding: EdgeInsets.all(2.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        CachedNetworkImage(
                          height: 120.0,
                          width: 200.0,
                          placeholder: Center(child: CircularProgressIndicator()),
                          imageUrl: simpleList[i].imageUrl,
                          fit: BoxFit.fill,
                        ),

                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            _getShortName(simpleList[i].name,19),
                            style: TextStyle(fontSize: 15.0,fontWeight: FontWeight.normal),),
                        ),
                        Container(
                          alignment: Alignment(-0.8, 0.0),
                          child: Text(simpleList[i].type,style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12.0,
                          ),
                          ),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Text('recipe by',
                                style: TextStyle(fontSize: 10.0,fontWeight: FontWeight.normal),
                              ),
                              SizedBox(
                                width: 1.0,
                              ),
                              Text(_getShortName(simpleList[i].author,14),
                                style: TextStyle(fontSize: 11.0,fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                width: 8.0,
                              )
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.orange[100],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Icon(Icons.thumb_up,size: 18.0,color: Colors.orange,),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text('${simpleList[i].like}',style: TextStyle(fontSize: 14.0),),
                            ),
                            SizedBox(
                              width: 6.0,
                            ),
//                            Padding(
//                              padding: const EdgeInsets.all(4.0),
//                              child: Icon(Icons.favorite,size: 14.0,color: Colors.red,),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.all(4.0),
//                              child: Text('${simpleList[i].favorite}'),
//                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              childCount: simpleList.length,
            ),
          ),
        ],
      ),
    );
  }

  String _getShortName(String val,int index){
    if(val.length > index){
      return "${val.substring(0,index)}...";
    }else{
      return val;
    }

  }


  _getModel(int index) async{
    List<RecipeSimpleModel> model = new List();
    String type = '';
    if(index == 0){
      type = 'Western';
    }else if(index == 1){
      type = 'Oriental';
    }else if(index == 2){
      type = 'Fusion';
    }
    QuerySnapshot querySnapshot;
    if(isSortByLike){
      querySnapshot = await Firestore.instance.collection('simpleRecipes').where('type',isEqualTo: type).orderBy('like',descending: true).getDocuments();
    }else{
      querySnapshot = await Firestore.instance.collection('simpleRecipes').where('type',isEqualTo: type).orderBy('time',descending: true).getDocuments();
    }

    querySnapshot.documents.forEach((document){
      model.add(HashMapManager().getSimpleModelFromMap(document));
    });
    setState(() {
      simpleList = model;
    });
  }


  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void _tappedSort() {
    setState(() {
      isSortByLike = !isSortByLike;
    });
    _getModel(_currentIndex);
  }
}

