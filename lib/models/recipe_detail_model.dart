import 'package:recipe_app/models/procedure_model.dart';

class RecipeDetail {

  String recipeName,author,imageUrl,recipeType,token,authEmail;
  int likeCount,favoriteCount;

  List<String> ingredientList;
  List<ProcedureModel> directionList;

  RecipeDetail(this.recipeName, this.author, this.imageUrl, this.recipeType,
      this.token, this.authEmail,this.likeCount, this.favoriteCount, this.ingredientList,
      this.directionList);


}