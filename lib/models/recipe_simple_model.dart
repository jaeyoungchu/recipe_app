import 'package:cloud_firestore/cloud_firestore.dart';

class RecipeSimpleModel {
  String name,type,author,imageUrl,token,authEmail;
  int like,favorite;
  DocumentReference reference;

  RecipeSimpleModel(this.name, this.type, this.author, this.imageUrl,this.token,this.authEmail, this.like, this.favorite);

  RecipeSimpleModel.fromMap(Map<String, dynamic> map, {this.reference})
      : name = map['name'],
        type = map['type'],
        author = map['author'],
        imageUrl = map['imageUrl'],
        token = map['token'],
        authEmail = map['authEmail'],
        like = map['like'],
        favorite = map['favorite'];

  RecipeSimpleModel.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);
}
